// time 2s sakyo ani call stack chai empty chaina bhane

// Anything that push its task to the background(node) are called asynchronous function
// During code execution, the background code will execute when all synchronous js code gets executed

// Call stack run the code inside it once the code gets executed, the code is popped off.

// Event loop is a mediator which continuously monitors call stack and memory queue. 
// If the call stack is empty it push the function from memory queue to call stack

console.log("1");
setTimeout(() => {
  console.log("Hello");
}, 0);
console.log("2");

