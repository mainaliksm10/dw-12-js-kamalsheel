setTimeout(() => {
  console.log("3s");
}, 3000);

setTimeout(() => {
  console.log("0");
}, 0);

setTimeout(() => {
  console.log("1");
}, 1000);

/* 
    any task that takes 5s
    => a

*/

/* 
    Jaba call stack ma asynchronous function aucha, tyo chai call stack le run garera background ma send gardincha.
    Background ma send garda kheri function along with timer pani huncha, ani tyo timer sakepachi memory queue ma jancha
    which works on FIFO principal 
*/
