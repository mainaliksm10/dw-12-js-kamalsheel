let ar1 = ["K", "a", "m", "a", "l"];

let ar2 = ar1.map((value, index) => {
  return value;
});

console.log(ar2);

let num1 = [1, 2, 3, 4, 5];

let num2 = num1.map((val, index) => {
  return val * 2;
});
console.log(num1);
console.log(num2);
