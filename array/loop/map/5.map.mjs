let ar1 = ["my", "name", "is"];

let ar2 = ar1.map((val, ind) => {
  return `${val.toUpperCase()}N`;
});

console.log(ar2);
