let num = [1, 2, 3, 4];
let displayResult = num.map((val, i) => {
  if (val % 2 === 0) {
    return val * 0;
  } else {
    return val * 2;
  }
});
console.log(displayResult);

let name = ["k", "a", "m", "a", "l", "s", "h", "e", "e", "l"];
let firstLetterCapital = name.map((val, i) => {
  if (i === 0) {
    return val.toUpperCase();
  }
  return val.toLocaleLowerCase();
});
console.log(firstLetterCapital);

let sentence = ["my", "name", "is"];

let newResult = sentence.map((val, i) => {
  return `${val.toUpperCase()}N`;
});
console.log(newResult);

let letters = ["a", "b", "c"];
let filterLetters = letters.filter((val, i) => {
  if (val === "a") {
    return true;
  } else if (val === "b") {
    return true;
  } else {
    return false;
  }
});
console.log(filterLetters);

let strings = ["a", "b", 23, "c", 2323, "d", 1, 2, 3, 4, 5];

let filterStrings = strings.filter((val, i) => {
  if (typeof val === "string") {
    return true;
  } else {
    return false;
  }
});
console.log(filterStrings);
