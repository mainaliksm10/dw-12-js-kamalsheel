let firstLetterCapital = (inputStr) => {
  let inputArr = inputStr.split("");
  let arr2 = inputArr.map((val, i) => {
    if (i === 0) {
      return val.toUpperCase();
    } else {
      return val.toLowerCase();
    }
  });

  let finalStr = arr2.join("");
  return finalStr;
};

let _firstLetterCapital = firstLetterCapital("kamaLSHEEL");
console.log(_firstLetterCapital);

