let ar1 = [1, 2, 3, 4];

let ar2 = ar1.map((val, ind) => {
  return val * 2;
});
console.log(ar2);

// Use map if:
//   input and output are array
//   input and output length are same
// [1,2,3,4,5]=[2,3,2,1,2]
// [1,2,3,4,5]=[2,3,2,1]
