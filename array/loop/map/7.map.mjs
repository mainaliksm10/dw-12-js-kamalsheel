let ar1 = [1, 3, 4, 5];

let ar2 = ar1.map((val, ind) => {
  if (val % 2 !== 0) {
    return val * 100;
  } else {
    return val;
  }
});

console.log(ar2);
