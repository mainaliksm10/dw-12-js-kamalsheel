/* 
[1,2,3,4] = [1,2] ok
[1,2,3,4] = [1,4] ok
[1,4] =[1,4,4] not ok
[1,2,3,4] = [1,5] not ok

By default return will be false 
*/

let ar1 = [1, 2, 3, 4, 5];

let ar2 = ar1.filter((val, ind) => {
  if (val % 2 === 0) {
    return true;
  } else {
    return false;
  }
});

console.log(ar2);
