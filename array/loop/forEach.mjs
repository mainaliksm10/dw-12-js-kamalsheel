let name = ["k", "a", "m", "a", "l", "s", "h", "e", "e", "l"];

name.forEach((value, index) => {
  console.log(`Value is: ${value} at index: ${index}`);
});

let name2 = ["n", "i", "t", "a", "n"];

name2.forEach((value, index) => {
  console.log(`${value}am,`);
});
