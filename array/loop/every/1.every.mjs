let ar1 = [9, 10, 12, 13, 32, 1223, 12323, 34323];

let isGreaterThan18 = ar1.every((value, index) => {
  if (value > 18) {
    return true;
  } else {
    return false;
  }
});
console.log(isGreaterThan18);

/* 
    Output will be true if all of the elements return true 
*/

let num1 = [2, 4, 9, 6];

let isEven = num1.every((val, i) => {
  if (val % 2 === 0) {
    return true;
  }
});

console.log(isEven);
