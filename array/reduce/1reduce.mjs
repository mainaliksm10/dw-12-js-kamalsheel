/* 
    syntax
    execution 
    uses
*/

// sum of all elements

let shoppingCart = [1000, 2000, 3000, 40000];

let totalPrice = shoppingCart.reduce((prev, curr) => {
  return prev + curr;
}, 0);

console.log(totalPrice);
