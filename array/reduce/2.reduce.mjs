let num = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

let newNum = num.reduce((prev, curr) => {
  return prev * curr;
}, 1);

console.log(newNum);
