const arr1 = ["a", "c", "d", "b", "f"];
console.log("Before sorting in ascending order:\n", arr1);
const arr2 = arr1.sort();
console.log(`After sorting the array is: ${arr2}`);



/* 
    Ascending Sort:
    [1,9] => [1,9]
    ["c","a"] => ["a","c"]
    [9,10]=> [10,9] INTERVIEW
    [12,16] => [12,16]
    ["a","b","A"] => ["A","a","b"] INTERVIEW 
*/
