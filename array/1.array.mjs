// Array is used to store data of different type or same type

let names = ["Kamalsheel", "Henry", "Jack"];

console.log(`Value at first index is: ${names[0]}`);
console.log(`Value at second index is: ${names[1]}`);
console.log(`All the names in the array is: ${names}`);

names[1] = "Petter";
console.log(`Modified name at second index is: ${names[1]}`);
