// There is no descending Sort in javascript, so we reverse the ascending sort
let arr1 = ["e", "a", "c", "d", "b", "A"];

let arr2 = arr1.sort().reverse();
console.log(arr2);
