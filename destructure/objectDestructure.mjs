let info = {
  name: "kamalsheel",
  age: 22,
  isMarried: false,
};

// Variable name must be same
// In object destructuring, order does not matter
let { name, age, isMarried } = {
  name: "kamalsheel",
  age: 22,
  isMarried: false,
};
console.log(name);
console.log(age);
console.log(isMarried);
