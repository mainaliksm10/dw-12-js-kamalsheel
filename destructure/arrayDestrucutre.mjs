let list = [1, 2, 3, 4];

console.log(list);

console.log("Another way to write array:");
let [a, b, c, d] = [1, 2, 3, 4];

console.log(a);
console.log(b);
console.log(c);
console.log(d);
