import { a } from "./3.dynamicLanguage.mjs";
import { address, college, school } from "./8.importExport.mjs";
import { age } from "./if_else/2.ifelse.mjs";
import isTest from "./if_else/2.ifelse.mjs";

console.log("My name is Kamalsheel Mainali");
console.log("Chabahil");
// console.log("My age is 22");
console.log("a");
// console.log("e");
// console.log("i");
// console.log("o");
console.log("u");

console.log(1 + 1);
console.log(4 * 5);
console.log(4 - 5);
console.log(4 / 5);
console.log(4 % 5);

console.log(1 + 1); // RESULT IS NUMBER
console.log("1" + "1"); // RESULT IS STRING
console.log(1 + "1"); // RESULT IS STRING

console.log("1" + 2 + 3 + 4);

// IMPORTING FROM 8.importExport.mjs

console.log(address);
console.log(school);
console.log(college);
console.log(age);
console.log(isTest);

console.log(a);
