let num = 5;
console.log(num);
num = null;
console.log(num);
console.log(typeof null);
/* 
    undefined means a variable is defined but not touched (initialized)
    null means a variable is defined and initialized to empty 
    typeof null is an object, which is a bug in javascript 
*/
