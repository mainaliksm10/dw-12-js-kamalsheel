let products = [
  {
    id: 1,
    title: "Product 1",
    category: "electronics",
    price: 5000,
    description: "This is description and Product 1",
    discount: {
      type: "other",
    },
  },
  {
    id: 2,
    title: "Product 2",
    category: "cloths",
    price: 2000,
    description: "This is description and Product 2",
    discount: {
      type: "a1",
    },
  },
  {
    id: 3,
    title: "Product 3",
    category: "electronics",
    price: 3000,
    description: "This is description and Product 3",
    discount: {
      type: "a2",
    },
  },
];

let ids = products.map((val, i) => {
  return val.id;
});
// console.log(ids);

let titles = products.map((val, i) => {
  return val.title;
});
// console.log(titles);

let categories = products.map((val, i) => {
  return val.category;
});
// console.log(categories);

let types = products.map((val, i) => {
  return val.discount.type;
});
// console.log(types);

let prices = products.map((val, i) => {
  return val.price * 3;
});
// console.log(prices);

let price3000 = products.filter((val, i) => {
  if (val.price >= 3000) return true;
});
// console.log(price3000);

// If filter and map are used simultaneously (always use filter first)
let priceTitle = products
  .filter((val, i) => {
    if (val.price >= 3000) return true;
  })
  .map((val, i) => {
    return val.title;
  });
// console.log(priceTitle);

let priceNot5000 = products
  .filter((val, i) => {
    if (val.price !== 5000) return true;
  })
  .map((val, i) => {
    return val.title;
  });
// console.log(priceNot5000);

let priceEquals3000 = products
  .filter((val, i) => {
    if (val.price === 3000) return true;
  })
  .map((val, i) => {
    return val.category;
  });
console.log(priceEquals3000);

/* 
  Map is used to modify input of elements whereas filter is used to filter the elements of input.
*/
