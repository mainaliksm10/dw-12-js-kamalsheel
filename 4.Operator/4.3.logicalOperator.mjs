// &&. ||, !
// && true if all are true

console.log("Using AND operator:");
console.log(true && true && true); //true
console.log(true && true && false); //false

console.log("Using OR operator:");
console.log(true || true || false); //true
console.log(false || false || false); //false

console.log("Using NOT operator:");
console.log(!true); //false
