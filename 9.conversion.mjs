// CONVERSION TO STRING :

console.log("CONVERSION TO STRING");
let num = 12345;
let strNum = String(num);
console.log(typeof strNum);
console.log(strNum);

// CONVERSION TO NUMBER
console.log("CONVERSION TO NUMBER");
console.log(strNum);
console.log(typeof strNum);
console.log(Number(strNum));

// CONVERSION TO BOOLEAN

console.log(Boolean("Kamalsheel Mainali")); // true
console.log(Boolean("a")); // true
console.log(Boolean("e")); //true
console.log(Boolean("")); // false
console.log(Boolean(" ")); // true
console.log(Boolean(0)); // false
console.log(Boolean(0.000000123)); //true
console.log(Boolean(100)); //true
console.log(Boolean(1)); //true

/* 
all empty are falsy value
string 
"" -> false
all are truthy 

number 
0-> falsy
all are truthy 
 */
