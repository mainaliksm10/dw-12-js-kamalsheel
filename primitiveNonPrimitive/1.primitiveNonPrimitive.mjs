/* 
    In case of primitive:
        A new memory space is created if let keyword is used.
    
    let a=1
    let b=a
    let c=1 
    a=10 

    console.log(a) // 10
    console.log(b) // 1
    console.log(c) // 1

        === (value):
            In primitive, the triple equals to produce true if the value are same.
    
    In case of non primitive:
        A new memory space is created if a variable is not copy of another variable. 
        If a variable is a copy of another variable, the variable shares the memory.

    let a =[1,2]
    let b=a
    let c=[1,2]
    a.push(10)

    console.log(a) //[1,2,10]
    console.log(b) //[1,2,10]
    console.log(c) //[1,2]

        === (value):
                In non primitive, the triple equals to produce true if they share rhe same memory location.

*/
