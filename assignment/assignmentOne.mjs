console.log("Task no: a");
let is18 = function (age) {
  if (age > 18) {
    return true;
  } else {
    return false;
  }
};
let testValue = is18(1);
console.log(testValue);

console.log("Task no: b");
let isGreaterThan18 = function (value) {
  if (value > 18) {
    return true;
  } else {
    return false;
  }
};
testValue = isGreaterThan18(99);
console.log(testValue);

console.log("Task no: c");
let enterRoom = function (randomNumber) {
  if (randomNumber > 18) {
    return true;
  }
  return false;
};
testValue = enterRoom(19);
if (testValue === true) {
  console.log("You can enter the room!");
} else {
  console.log("You cannot enter the room!");
}

console.log("Task no: d");
let isEven = function (number) {
  if (number % 2 === 0) {
    return true;
  }
  return false;
};
testValue = isEven(888);
if (testValue === true) {
  console.log(`The number is even`);
} else {
  console.log(`The number is odd`);
}

console.log("Task no: e");
let average = function (num1, num2, num3) {
  return (num1 + num2 + num3) / 3;
};
testValue = average(10, 20, 300);
console.log(`The average of three numbers is: ${testValue}`);

console.log("Task no: f");
let rangeCategory = function (num) {
  if (num >= 1 && num <= 10) {
    return "category1";
  } else if (num >= 11 && num <= 20) {
    return "category2";
  } else if (num >= 21 && num <= 30) {
    return "category3";
  }
};
testValue = rangeCategory(30);
if (testValue === "category1") {
  console.log("category1");
} else if (testValue === "category2") {
  console.log("category2");
} else if (testValue === "category3") {
  console.log("category3");
}

console.log("Task no: g");
let ticketPrice = function (age) {
  if (age >= 1 && age <= 17) {
    return "free";
  } else if (age >= 18 && age <= 25) {
    return "100";
  } else if (age > 26) {
    return "200";
  } else if (age === 26) {
    return "150";
  }
};
testValue = ticketPrice(26);
if (testValue === "free") {
  console.log("Your ticket is free");
} else if (testValue === "100") {
  console.log("Your ticket is Rs.100");
} else if (testValue === "200") {
  console.log("Your ticket is Rs.200");
} else if (testValue === "150") {
  console.log("Your ticket is Rs.150");
}

console.log("Task no: h");
let takeANumber = function (number) {
  if (number > 3) {
    return "firstCase";
  } else if (number === 3) {
    return "secondCase";
  } else if (number < 3) {
    return "thirdCase";
  } else {
    return "other";
  }
};
testValue = takeANumber(3);
if (testValue === "firstCase") {
  console.log("I am greater or equal to 3");
} else if (testValue === "secondCase") {
  console.log("I am 3");
} else if (testValue === "thirdCase") {
  console.log("I am less than 3");
} else {
  console.log("I am other");
}

console.log("Task no: i");
let watchMovie = function (age) {
  if (age >= 18) {
    return true;
  } else {
    return false;
  }
};
testValue = watchMovie(8);
if (testValue === true) {
  console.log("You can watch movies");
} else {
  console.log("You are not authorized to watch movies");
}


console.log("Hello")
console.log("a")
console.log("b")
console.log("f")