/* let a = "kamalsheel";
a = 'Kamalsheel Main"ali"';
console.log(a);
 */

let firstName = "Kamalsheel";
let lastName = "Mainali";
let fullName = `My first name is ${firstName} and my last name is ${lastName}`;
console.log(fullName);

/* 

    string can be defined using ' ', " ", ` `
    we can call variable inside `` using ${} but it's not possible in ''. ""
    we can not use '' inside '' and so on

*/


