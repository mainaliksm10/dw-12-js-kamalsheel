let sum = function (num1, num2) {
  let num3 = num1 + num2;
};

let add = sum(1, 2);
console.log(add);

/* 
    CONCLUSION:
            function without return call => sum()
            function with return call => let a =sum()
*/