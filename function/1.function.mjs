let fun1 = function () {
  let num1 = 12;
  let num2 = 342;
  console.log(num1 * num2);
};
console.log("Hello");
fun1();
console.log("Out of the function");
fun1();
console.log("Again entering the function");
fun1();
console.log("Entering the function for the final time");
fun1();
