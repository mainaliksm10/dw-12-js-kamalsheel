
/* 
    object is used to store multiple data
    object is same as array but it has information of a data.
    It has 3 parts:
    property = key + value
*/

let info = {
  name: "Kamalsheel",
  age: 22,
  weight: 74,
  isMarried: false,
};

console.log(info);
info.weight = 73;
console.log(info);
delete info.isMarried;
console.log(info);
