let info = {
  firstName: "Kamalsheel",
  lastName: "Mainali",
  age: 22,
  fullName: () => {
    console.log(`My full name is: ${info.firstName} ${info.lastName}`);
  },
};
console.log(info.firstName);
info.fullName();

// this always points itself
// Arrow function does not support this
