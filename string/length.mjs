let name = "Kamalsheel Mainali";

let nameLength = name.length;
console.log(`The length of string ${name} is ${name.length}`);
console.log(`Use of toUpperCase(): ${name.toUpperCase()}`);
console.log(`Use of toLowerCase(): ${name.toLowerCase()}`);

name = "       Kamalsheel Mainali";
console.log(`Beofre trim: ${name} \nAfter trim: ${name.trimStart()}`);
console.log(name.startsWith(" "));
console.log(name.endsWith("Mainali"));
console.log(name.replace("K", "a")); // replace method will only replace the first match
console.log(name.replaceAll("a", "s")); // replaceAll method will replace all match

let myTrim = (name) => {
  let newName = name.trim();
  return newName;
};

let nname = myTrim("   Kamalsheel");
console.log(nname);
