/* 

    () -> Parenthesis
    {} -> Curly braces //block
    [] -> array

*/

// if block gets executed if condition is true

if (true) {
  console.log("Hello, I am if");
}

let name = "Kamalsheel Mainali";

if (name === "ram") {
  console.log("Hello, my name is Ram!");
}

let a = "0";
if (a) {
  console.log("Hello, this is true block");
}
